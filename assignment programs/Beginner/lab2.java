//swap the two numbers
package Beginner;

public class lab2
{
 public static void SwapNum(int num1,int num2 )
 { 
	 int temp ;
	 
	 System.out.println("the first value is "+num1);
	 System.out.println("the second value is "+num2);
	 
	 System.out.println("After done swapping");
	 System.out.println();
	 
	 //swapping logic
	 temp = num1;
	 num1 = num2;
	 num2 = temp;
	 
	 //displaying
	 System.out.println("the first value is "+num1);
	 System.out.println("the second value is "+num2);
	 
	 
 }
	public static void main(String[] args) 
	{
		SwapNum(15, 34);
	}

}
