//reverse num using while loop
package Intermediate;

public class lab3 {

	public static void main(String[] args) 
	{
		int num = 145689;
		int rev = 0;
		while(num!=0)
		{
			rev = rev*10 + num%10;
			num = num/10;		
		}
    System.out.println(rev);
	}

}
