//program to-find-the-smallest-of-three-numbers-using-ternary-operator
package Intermediate;

public class lab9 {

	public static void main(String[] args) 
	{
		//declaration
		int a,b,c,temp,result;
		
		//initialization 
		a = 17;
		b = 10;
		c = 11;
		
		//ternary operator
		temp = a < b ? a:b;
		result = c < temp ? c:temp;
		
		System.out.println("the smallest number is "+result);

	}

}
