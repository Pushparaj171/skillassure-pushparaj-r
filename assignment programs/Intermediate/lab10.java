//to print the fibonaci series upto N 
package Intermediate;

public class lab10 {

	public static void main(String[] args)
	{
		//step1 declaration
		int n1,n2, sum;
		
		//initialization 
		n1 = 0;
		n2 = 1;
		sum = 0;
		int N =10; 
		
		//display n1 and n2
		System.out.print(n1+","+n2+",");
		
		//iteration
		for(int i=2;i<=N;i++)
		{
			sum = n1 + n2;
			System.out.print(sum+",");
			n1 = n2;
			n2 = sum;
			
		}
	}

}
