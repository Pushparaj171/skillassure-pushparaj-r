package advanced;

import java.util.Scanner;

public class TaxCalculation
{

	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the name Of employee");
		String name = sc.next();
		System.out.println("Enter the employee ID");
		int ID =sc.nextInt();
		System.out.println("Enter the Bsic Salary");
		double basicSalary = sc.nextDouble();
		System.out.println("Enter the special allowance");
		double specialAllowance =sc.nextDouble();
		System.out.println("Enterr the percentage of bonus");
		double bonusPercent =sc.nextDouble();
		System.out.println("enter the monthly tax saving investment");
		double taxSavingInvest =sc.nextDouble();
		
		double grossMonthlySal = basicSalary+specialAllowance;
		System.out.println(grossMonthlySal);
		double annualSalary = grossMonthlySal *12;
		bonusPercent =annualSalary*bonusPercent;
		double grossAnnualSal = annualSalary  + bonusPercent;
		System.out.println("the gross Annual salary of "+name+" "+grossAnnualSal);
		double taxDeducted=0;
		double netAnnualSal =0;
		if(grossAnnualSal<=250000)
		{
			netAnnualSal = grossAnnualSal;
		}
		else if((grossAnnualSal>250000)&&(grossAnnualSal<=500000))
		{
			taxDeducted = grossAnnualSal *(0.05);
			netAnnualSal = grossAnnualSal -taxDeducted;
		}
		else if((grossAnnualSal>500000)&&(grossAnnualSal<=100000))
		{
			taxDeducted = grossAnnualSal *(0.20);
			netAnnualSal = grossAnnualSal -taxDeducted;
		}
		else
		{
			taxDeducted = grossAnnualSal *(0.30);
			netAnnualSal = grossAnnualSal -taxDeducted;
		}
		System.out.println("the net annual salary "+name+" "+netAnnualSal);
	}

}
