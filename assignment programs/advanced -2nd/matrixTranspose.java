package advanced;

public class matrixTranspose 
{
 public static void main(String[] args)
 {
	int row=3,column=4;
	int matrix [][] = {{2,3,4,5},{5,6,7,7},{2,6,2,2}};
	
	//display matrix
	display(matrix);
	
	//transpose the matrix
	int [][] transpose = new int[column][row];
	for(int i=0;i<row;i++)
	{
		for(int j=0;j<column;j++)
		{
			transpose [j][i] = matrix [i][j];
		}
	}
	
	//diplaying transpose
	display(transpose);
	
 }
 public static void display(int[][] matrix)
 {
	 System.out.println("The matrix is");
	 
	 for(int[] row:matrix)
	 {
		 for(int column:row)
		 {
			 System.out.print(column+" ");
		 }
		 System.out.println();
	 }
 }
}
