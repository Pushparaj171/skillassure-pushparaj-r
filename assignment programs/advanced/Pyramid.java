package advanced;

public class Pyramid 
{

	public static void main(String[] args) 
	{
		int n = 7;
		int str = 1;
		int spc =n-1;
		
		for(int i =1;i<=n;i++)
		{
			for(int j=1;j<=spc;j++)
			{
				System.out.print(" "+" ");
			}
			for(int k=1;k<=str;k++)
			{
				System.out.print("*"+" ");
			}
			System.out.println();
			str = str+2;
			spc--;
		}
	}
}
