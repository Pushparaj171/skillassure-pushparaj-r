package advanced;

import java.util.Scanner;

public class LinearSearch
{

	public static void main(String[] args) 
	{
		Scanner scn = new Scanner(System.in);
		System.out.println("Enter the Size of an array");
		int N =scn.nextInt();
		
		//getting the array value from user
		int [] array = new int[N];
		
		for(int i=0;i<array.length;i++) 
		{
			System.out.println("Enter the value for "+"array["+i+"]");
			array[i] = scn.nextInt();
		}
		
		//to display the array
		display(array);
		
		//searching the key where key got from user
		System.out.println("Enter key to find ");
		int key=scn.nextInt();
		int result = linearsearch(array,key);
		if(result != -1)
			System.out.println("Element found at the index "+result+" in an array");
		else
			System.out.println("Element not  found");
		scn.close();
	
		
		
	}
	static void display(int[] array)
	{
		System.out.println("the array is : ");
		for(int i=0;i<array.length;i++)
		{
			System.out.print(array[i]+",");
		}
		System.out.println();
	}
	static int linearsearch(int[] array,int key)
	{
		for(int i=0;i<array.length;i++)
		{
			if(array[i]==key)
				return i;
		}
		return -1;
	}
}
