package advanced;

public class matrix 
{

	public static void main(String[] args) 
	{
		int row = 2;
		int column = 3;
		int[][] matrix = {{10,2,56},{12,31,12}};
		
		//creating obj
		 DisplayMatrix displayMatrix = new DisplayMatrix();
		//displaying matrix
		 displayMatrix.display(matrix);
		
		//transpose logic
		int[][] transpose = new int[column][row];
		
		//iterating 
		for(int i=0;i<row;i++)
		{
			for(int j=0 ;j<column;j++)
			{
				transpose [j][i] = matrix[i][j];
			}
		}
		
		//displaying transpose matrix
		displayMatrix.display(transpose);
	}
	
}
class DisplayMatrix
{
	void display(int[][] matrix)
	{
		System.out.println("the matrix is :");
		for(int[] row: matrix)
		{
			for(int column:row)
			{
				System.out.print(column+" ");
			}
			System.out.println();
		}
}
	}