package impques;

import java.util.Scanner;

public class StudentPercentage 
{
	public static void main(String[] args) 
	{
		//goal to find the percentage and average of the marks 
		
		//Step1 declaring the variables 
			String Name = "";
			int Eng = 0;
			int Tam = 0;
			int Math = 0;
			double Average = 0;
			int Tot = 0;
			
		//step 2 getting values form the user 
			Scanner scn = new Scanner(System.in);
			System.out.println("Enter the Name of the student");
			Name = scn.next();
			System.out.println("Enter the Eng Mark");
			Eng = scn.nextInt();
			System.out.println("Enter the Tamil Mark");
			Tam = scn.nextInt();
			System.out.println("Enter the Maths Mark");
			Math = scn.nextInt();
			scn.close();
			
		//step3 calculating the Average AND total
			Tot =  Eng + Tam + Math;
			Average = Tot/3;
			
		// displaying average and tot
			System.out.println("the total Scored by "+Name +" is "+Tot);
			System.out.println("the Average Scored by "+Name +" is "+Average);
			
		//to determine the class
			if(Eng>=35 && Tam>=35 && Math>=35 )
			{
				if(Average>=60)
					System.out.println(Name+" is "+"firstclass");
				else if(Average>=50 )
					System.out.println(Name+" is "+"second class");
				else
					System.out.println(Name+" is "+"Justpass");
			}
			else
			{
				System.out.println(Name+" is "+"fail");
			}
			
	}
}
